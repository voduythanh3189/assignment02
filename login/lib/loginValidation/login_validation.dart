
import 'dart:async';

// class LoginValidation {
//   static bool isValidEmail (String email)
//   {
//     return email != null && email.length > 6 && email.contains("@");
//   }
//   static bool isValidPassword (String pass)
//   {
//     return pass != null && pass.length > 6;
//   }
// }

mixin LoginValidation {
  var emailValidator =
      StreamTransformer<String,String>.fromHandlers(handleData: (email, sink) {
      Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (null != email && regex.hasMatch(email))
      sink.add(email);
    else
      sink.addError("Invalid Email!");
  });

  var passwordValidator =
      StreamTransformer<String,String>.fromHandlers(handleData: (password, sink) {
    if (null != password && password.length >= 6)
      sink.add(password);
    else
      sink.addError("Passwords must be at least 6 characters long");
  });
}