
import 'dart:async';
import 'package:login/bloc/base_bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:login/loginValidation/login_validation.dart';

class LoginBloc extends Object with LoginValidation implements BaseBLoC{

  StreamController<String> _emailController = new BehaviorSubject<String>();
  StreamController<String> _passController = new BehaviorSubject<String>();

  Stream<String> get emailStream =>_emailController.stream.transform(emailValidator);
  Stream<String> get passwordStream =>_passController.stream.transform(passwordValidator);

  StreamSink<String> get emailValueChanged => _emailController.sink;
  StreamSink<String> get passValueChanged => _passController.sink;

    Stream<String> get enableButton => Observable.combineLatest2(emailStream,passwordStream, (email, pass) => "OK");

  // bool isValidInfo(String email, String password){
  //   if(!LoginValidation.isValidEmail(email)){
  //     _emailController.sink.addError('Invalid Email');
  //     return false;
  //   }
  //   _emailController.sink.add('OK');
  //   if(!LoginValidation.isValidPassword(email)){
  //     _passController.sink.addError('Invalid Password');
  //     return false;
  //   }
  //   _passController.sink.add('OK');
  //   return true;
  // }

  void dispose(){
    _emailController.close();
    _passController.close();
  }

}