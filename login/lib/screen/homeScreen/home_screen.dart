import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  HomeScreenSate createState() => HomeScreenSate();
}

class HomeScreenSate extends State<HomeScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text(
          'Home',
          style: new TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: Colors.green,
      ),
      body: new SafeArea(
          child: Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          padding: const EdgeInsets.only(left: 15.0, right: 15.0),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new AnimatedOpacity(
                  opacity: 1.0,
                  duration: new Duration(seconds:2),
                  child: new Container(
                    margin: const EdgeInsets.only(bottom: 30.0),
                    width: double.infinity,
                    height: 100.0,
                    child: new Image.network(
                      'https://cdn-images-1.medium.com/max/1600/1*ogNimrTnf_COR7ImVFr3-A.png'
                    ),
                  ),
                ),
                new Container(

                  width: double.infinity,
                  height: 40.0,
                  child: new Text(
                    'thanhvo@gmail.com',
                    textAlign: TextAlign.center,
                  ),
                )
              ],
            ),
        ), 
      ),
    );
  }
}
