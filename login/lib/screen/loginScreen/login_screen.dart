import 'package:flutter/material.dart';
import 'package:login/bloc/login_bloc.dart';
import 'package:login/screen/homeScreen/home_screen.dart';

class LoginScreen extends StatefulWidget {
  @override
  LoginScreenSate createState() => LoginScreenSate();
}

class LoginScreenSate extends State<LoginScreen> {
  LoginBloc bloc = new LoginBloc();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  void _btnLogin() {
    Navigator.of(context)
        .push(new MaterialPageRoute(builder: (BuildContext) => HomeScreen()));
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new SafeArea(
        child: Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          padding: const EdgeInsets.only(left: 15.0, right: 15.0),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Container(
                width: double.infinity,
                child: StreamBuilder<String>(
                    stream: bloc.emailStream,
                    builder: (context, snapshot) {
                      return TextField(
                        onChanged: (value)=> bloc.emailValueChanged.add(value),
                        decoration: InputDecoration(
                          labelText: "Email",
                          hintText: "Input your email here!",
                          errorText: snapshot.hasError ? snapshot.error : null,
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.zero,
                            borderSide:
                                BorderSide(color: Colors.green, width: 1.5),
                          ),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.zero),
                        ),
                      );
                    }),
              ),
              new Container(
                //height: 60.0,
                margin: const EdgeInsets.only(top: 15.0,bottom: 15.0),
                width: double.infinity,
                child: StreamBuilder<String>(
                    stream: bloc.passwordStream,
                    builder: (context, snapshot) {
                      return TextField(
                        onChanged: (value)=> bloc.passValueChanged.add(value),
                        obscureText: true,
                        decoration: InputDecoration(
                          labelText: "Password",
                          hintText: "Input your password here!",
                          errorText: snapshot.hasError ? snapshot.error : null,
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.zero,
                            borderSide:
                                BorderSide(color: Colors.green, width: 1.5),
                          ),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.zero),
                        ),
                      );
                    }),
              ),
              new Container(
                width: double.infinity,
                child: StreamBuilder<String>(
                  stream: bloc.enableButton,
                  builder: (context, snapshot) {
                    return new RaisedButton(
                      elevation: 0.0,
                      onPressed:snapshot.hasData ? ()=>_btnLogin() : null,
                      color: Colors.green,
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(0.0),
                      ),
                      child: new Text(
                        'Login',
                        style: new TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
