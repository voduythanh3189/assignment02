import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:login/screen/loginScreen/login_screen.dart';

void main() 
{
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
    .then((_) {
      runApp(new MyApp());
    });
}

//=> runApp(MyApp());

class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      //title: 'World Golf Network',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        primaryColorBrightness: Brightness.light
      ),
      home:LoginScreen(),
    );
  }
}

